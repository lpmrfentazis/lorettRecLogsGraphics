from pydantic import BaseModel
from annotated_types import Annotated, MinLen, MaxLen, Le, Lt, Ge
from datetime import datetime



class BaseLogLineModel(BaseModel):
    timeMoment: datetime
    azimuth: Annotated[float, Ge(0), Lt(360)]
    elevation: Annotated[float, Ge(0), Le(90)]
    level: float
    snr: Annotated[float, Ge(0)]
    BER: Annotated[float, Ge(0)] = 1
