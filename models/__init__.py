from .lorett.lorettLogModel import LorettLogModel
from .scanex.scanexLogModel import ScanexLogModel


logsTypes = {
    LorettLogModel.__type__: LorettLogModel,
    ScanexLogModel.__type__: ScanexLogModel
    
}