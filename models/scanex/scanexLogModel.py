from pydantic import BaseModel, IPvAnyAddress
from annotated_types import Annotated, MinLen, MaxLen, Le, Lt, Ge
from datetime import datetime

from ..baseLogModel import BaseLogModel
from .scanexLogLineModel import ScanexLogLineModel



class ScanexLogModel(BaseLogModel):
    __type__ = "scanex"
    demodulator: Annotated[str, MinLen(1), MaxLen(50)]
    recieverID: Annotated[str, MinLen(1), MaxLen(50)]
    
    plannerVersion: Annotated[str, MinLen(1), MaxLen(50)]
    antennaVersion: Annotated[str, MinLen(1), MaxLen(50)]
    recieverVersion: Annotated[str, MinLen(1), MaxLen(50)]
    
    hostName: Annotated[str, MinLen(1), MaxLen(50)]
    ip0: IPvAnyAddress
    ip1: IPvAnyAddress
    demodIP: IPvAnyAddress
    username: Annotated[str, MinLen(1), MaxLen(50)]
    domain: Annotated[str, MinLen(0), MaxLen(50)]
    SID: Annotated[str, MinLen(1), MaxLen(50)]
    
    # format: Annotated[str, MinLen(1), MaxLen(50)] # configuration eq
    modulation: Annotated[str, MinLen(1), MaxLen(50)]
    clock: Annotated[float, Ge(0)] # in Mhz
    carrier: Annotated[float, Ge(0)] # in Mhz
    frameSize: Annotated[float, Ge(0)]
    BERBase: Annotated[float, Ge(0)]
    
    FEC: Annotated[str, MinLen(1), MaxLen(50)]
    swappedIQ: bool
    invertedI: bool
    invertedQ: bool
    scrambling: bool
    diffCoding: bool
    
    frequencyOffset: Annotated[float, Ge(0)]
    frequency: Annotated[float, Ge(0)]
    
    diskFree: Annotated[float, Ge(0)] # GiB
    firstBuffSize: Annotated[float, Ge(0)] # MiB
    secondBuffSize: Annotated[float, Ge(0)] # MiB
    
    lines: list[ScanexLogLineModel]