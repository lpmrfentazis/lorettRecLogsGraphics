from pydantic import BaseModel
from annotated_types import Annotated, MinLen, MaxLen, Le, Lt, Ge
from datetime import datetime

from ..baseLogLineModel import BaseLogLineModel

    
class ScanexLogLineModel(BaseLogLineModel):
    q: Annotated[str, MaxLen(1), MinLen(1)]
    no: Annotated[int, Ge(0)]
    scan: Annotated[int, Ge(0)]
    gain: Annotated[int, Ge(0)]
    
    frequency: Annotated[float, Ge(0)]
    lost: Annotated[int, Ge(0)]
    mod: str