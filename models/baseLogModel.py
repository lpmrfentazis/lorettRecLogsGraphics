from pydantic import BaseModel
from annotated_types import Annotated, MinLen, MaxLen, Le, Lt, Ge
from datetime import datetime

from .baseLogLineModel import BaseLogLineModel


class BaseLogModel(BaseModel):
    station: Annotated[str, MinLen(1), MaxLen(50)]
    
    lat: Annotated[float, Ge(-90), Le(90)]
    lon: Annotated[float, Ge(-180), Le(180)]
    
    TLE1: Annotated[str, MinLen(69), MaxLen(69)]
    TLE2: Annotated[str, MinLen(69), MaxLen(69)] 
    
    satellite: Annotated[str, MinLen(1), MaxLen(50)]
    configuration: Annotated[str, MinLen(1), MaxLen(50)]
    
    startTime: datetime
    
    lines: list[BaseLogLineModel]