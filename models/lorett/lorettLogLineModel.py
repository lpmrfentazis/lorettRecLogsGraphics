from pydantic import BaseModel
from annotated_types import Annotated, MinLen, MaxLen, Le, Lt, Ge
from datetime import datetime

from ..baseLogLineModel import BaseLogLineModel


class LorettLogLineModel(BaseLogLineModel):
    level1: float
    level2: float
    blocksErr: Annotated[float, Ge(0)]
    temperature: Annotated[float, Ge(0)]
    state: int