from pydantic import BaseModel
from annotated_types import Annotated, MinLen, MaxLen, Le, Lt, Ge
from datetime import datetime

from ..baseLogModel import BaseLogModel
from .lorettLogLineModel import LorettLogLineModel



class LorettLogModel(BaseLogModel):
    __type__ = "lorett"
    passID: Annotated[str, MinLen(17), MaxLen(67)] # YYYYmmdd_HHMMSS_{satellite}
    almurnet: Annotated[str, MinLen(1), MaxLen(50)]
    
    version: Annotated[str, MinLen(1), MaxLen(50)]
    
    lines: list[LorettLogLineModel]