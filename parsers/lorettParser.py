from pathlib import Path
from models.baseLogModel import BaseLogModel
from .baseParser import BaseParser

from models.lorett.lorettLogModel import LorettLogModel
from models.lorett.lorettLogLineModel import LorettLogLineModel

from datetime import datetime

import re



class LorettParser(BaseParser):
    name = "lorett"
    
    def parse(self, file: Path) -> LorettLogModel:
        # TODO update this parser pls...
        
        logLines = []
        passID = ""
        satellite = ""
        configuration = ""
        almurnet = ""
        startTime = datetime.now()
        version = ""
        station = ""
        lat = 0
        lon = 0
        TLE1 = ""
        TLE2 = ""
        
        with open(file) as f:
            for line in f:
                line = line.strip()
                try:
                    if "#Pass ID:" in line:
                        passID = line.split(self.config.keySeparator)[-1]
                    
                    elif "#Satellite:" in line:
                        satellite = line.split(self.config.keySeparator)[-1]
                    
                    elif "#Configuration:" in line:
                        configuration = line.split(self.config.keySeparator)[-1]
                    
                    elif "#almurnet" in line:
                        almurnet = line.split(self.config.keySeparator)[-1]
                    
                    elif "#Start time:" in line:
                        text = line.split(self.config.keySeparator)[-1]
                        startTime = datetime.strptime(text, self.config.timeStartFormat)
                    
                    elif "#Version:" in line:
                        version = line.split(self.config.keySeparator)[-1]
                    
                    elif "#Station:" in line:
                        station = line.split(self.config.keySeparator)[-1]
                    
                    elif "#Location:" in line:
                        text = line.split(self.config.keySeparator)[-1]
                        match = re.search(self.config.lonlatPattern, text)
                        
                        if match:
                            lon = float(match.group(1))
                            lat = float(match.group(2))
                            
                    elif "#TLE:" in line:
                        if TLE1 == "":
                            TLE1 = line.split(self.config.keySeparator)[-1]
                        else:
                            TLE2 = line.split(self.config.keySeparator)[-1]
                    
                    elif "#Time" in line:
                        continue        
                    
                    else:
                        parts = line.split(self.config.elementSeparator)
                        if len(parts) < 10:
                            continue
                        
                        timeMoment = datetime.strptime(parts[0], self.config.timeFormat)
                        azimuth = float(parts[1])
                        elevation = float(parts[2])
                        level1 = float(parts[3])
                        level2 = float(parts[4])
                        snr = float(parts[5])
                        BER = float(parts[6])
                        blockErr = int(parts[7])
                        temperature = int(parts[8])
                        state = int(parts[9])
                        
                        logLines.append(LorettLogLineModel(timeMoment=timeMoment,
                                                azimuth=azimuth,
                                                elevation=elevation,
                                                level=level1,
                                                level1=level1,
                                                level2=level2,
                                                snr=snr if snr > self.config.toZiroWhenLoner else 0,
                                                BER=BER,
                                                blocksErr=blockErr,
                                                temperature=temperature,
                                                state=state))
                        
                except Exception as e:
                    print(e)
                
        return LorettLogModel(passID=passID,
                        satellite=satellite,
                        configuration=configuration,
                        almurnet=almurnet,
                        startTime=startTime,
                        version=version,
                        station=station,
                        lat=lat,
                        lon=lon,
                        TLE1=TLE1,
                        TLE2=TLE2,
                        lines=logLines)