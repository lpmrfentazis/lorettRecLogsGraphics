from .baseParser import BaseParser
from .lorettParser import LorettParser
from .scanexParser import ScanexParser
from config import LogFormat


supported = [
    LorettParser,
    ScanexParser
]

def getParser(format: LogFormat) -> BaseParser:
    for Parser in supported:
        if Parser.name == format.name:
            return Parser(format)
    
    raise NotImplementedError(f"Parser with name {format.name} is not supported")