from pathlib import Path
from models.baseLogModel import BaseLogModel
from .baseParser import BaseParser

from models.scanex.scanexLogModel import ScanexLogModel
from models.scanex.scanexLogLineModel import ScanexLogLineModel

from datetime import datetime, time, timedelta
import re



class ScanexParser(BaseParser):
    name = "scanex"
    
    def parse(self, file: Path) -> ScanexLogModel:
        logLines = []
        demodulator = ""
        recieverID = ""
        plannerVersion = ""
        antennaVersion = ""
        recieverVersion = ""
        
        hostName = ""
        ip0 = ""
        ip1 = ""
        demodIP = ""
        
        username = ""
        
        domain = ""
        SID = ""
        
        format = ""
        modulation = ""
        clock = 0.0
        carrier = 0.0
        frameSize = 0.0
        BERBase = 0.0
        
        FEC = ""
        swappedIQ = False
        invertedI = False
        invertedQ = False
        scrambling = False
        diffCoding = False
        
        frequencyOffset = 0
        frequency = 0
        
        diskFree = 0
        firstBuffSize = 0
        secondBuffSize = 0
        
        satellite = ""
        startTime = datetime.now()
        station = ""
        lat = 0
        lon = 0
        TLE1 = ""
        TLE2 = ""
        
        startDatePart = ""
        
        with open(file) as f:
            for line in f:
                line = line.strip()
                try:
                    if "Station:" in line:
                        name, position = line.split(self.config.keySeparator, 1)[1].split(" ", 1)
                        station = name.split('"')[1] # MTS_Rec_Dem500 from "MTS_Rec_Dem500"
                        
                        m = re.search(self.config.lonlatPattern, position)
                        
                        if m:
                            lon = float(m.group(2))
                            lat = float(m.group(1))
                    
                    if "Demodulator:" in line:
                        demodulator = line.split(self.config.keySeparator)[-1]
                    
                    elif "Receiver ID:" in line:
                        recieverID = line.split(self.config.keySeparator)[-1]
                    
                    elif "Planner app. version:" in line:
                        plannerVersion = line.split(self.config.keySeparator)[-1]
                    
                    elif "Antenna app. version:" in line:
                        antennaVersion = line.split(self.config.keySeparator)[-1]
                    
                    elif "Receiver app. version:" in line:
                        recieverVersion = line.split(self.config.keySeparator)[-1]
                    
                    elif "Host name:" in line:
                        hostName = line.split(self.config.keySeparator)[-1]
                       
                    elif "IP address#0:" in line:
                        ip0 = line.split(self.config.keySeparator)[-1]
                        
                    elif "IP address#1:" in line:
                        ip1 = line.split(self.config.keySeparator)[-1]
                    
                    elif "Demod IP address#01:" in line:
                        demodIP = line.split(self.config.keySeparator)[-1]
                    
                    elif "User name:" in line:
                        username = line.split(self.config.keySeparator)[-1]
                    
                    elif "Domain:" in line:
                        domain = line.split(self.config.keySeparator)[-1]
                    
                    elif "SID:" in line:
                        SID = line.split(self.config.keySeparator)[-1]
                    
                    elif "Satellite:" in line:
                        satellite = line.split(self.config.keySeparator)[-1]
                        
                    elif "Format:" in line:
                        format = line.split(self.config.keySeparator)[-1]
                    
                    elif "Mod. type:" in line:
                        modulation = line.split(self.config.keySeparator)[-1]
                    
                    elif "Clock:" in line:
                        text = line.split(self.config.keySeparator)[-1]
                        clock = float(text.split()[0]) # 7000.000 from "7000.000 MHz"
                        
                    elif "Carrier:" in line:
                        text = line.split(self.config.keySeparator)[-1]
                        carrier = float(text.split()[0]) # 7000.000 from "7000.000 MHz"
                        
                    elif "Frame size:" in line:
                        frameSize = int(line.split(self.config.keySeparator)[-1])
                        
                    elif "BER base:" in line:
                        BERBase = float(line.split(self.config.keySeparator)[-1].split()[0]) # 5.86E+4 from "5.86E+4 (bit/s)"
                    
                    elif "FEC:" in line:
                        FEC = line.split(self.config.keySeparator)[-1]
                    
                    elif "Swapped I/Q:" in line:
                        #FIXME
                        swappedIQ = bool(line.split(":")[-1])
                    
                    elif "Inverted I:" in line:
                        #FIXME
                        invertedI = bool(line.split(":")[-1])
                        
                    elif "Inverted Q:" in line:
                        #FIXME
                        invertedQ = bool(line.split(":")[-1])
                        
                    elif "Scrambling:" in line:
                        #FIXME
                        scrambling = bool(line.split(":")[-1])
                        
                    elif "Dif. coding:" in line:
                        #FIXME
                        diffCoding = bool(line.split(":")[-1])
                        
                    elif "LO freq:" in line:
                        text = line.split(self.config.keySeparator)[-1]
                        frequencyOffset = float(text.split()[0]) # 7000.000 from "7000.000 MHz"
                    
                    elif "Input freq.:" in line:
                        text = line.split(self.config.keySeparator)[-1]
                        frequency = float(text.split()[0]) # 1160.000 from "1160.000 MHz"
                    
                    elif "Disk free space:" in line:
                        diskFree = int(line.split(self.config.keySeparator)[-1].split("GiB")[0]) # 954 from "954GiB"
                    
                    elif "1st byffer size:" in line:
                        firstBuffSize = int(line.split(self.config.keySeparator)[-1].split("MiB")[0]) # 33 from "33MiB"
                        
                    elif "2nd byffer size:" in line:
                        secondBuffSize = int(line.split(self.config.keySeparator)[-1].split("MiB")[0]) # 33 from "33MiB"
                    
                    elif "Date:" in line:
                        # The scannex only keeps the time in the lines. 
                        # The date is specified in a separate field.
                        # We will neglect the situation when a new date occurs during the passage 
                        startDatePart = line.split(self.config.keySeparator)[-1]
                    
                    elif "Time:" in line:
                        startTimePart = line.split(self.config.keySeparator)[-1]
                    
                    elif line[:2] == "1 ":
                        TLE1 = line
                    
                    elif line[:2] == "2 ":
                        TLE2 = line
                    
                    parts = line.split()
                    # count of elements in scannex log line
                    # N     1  10:12:13        0  140  05    787    728  1160.000   0.0       0       N/D     SQPSK
                    if len(parts) != 13: 
                        continue
                    
                    # skip head
                    # Q    No      Time     Scan   Az  El   Gain  Level      Freq   SNR    Lost       BER       MOD
                    if parts[-1] == "MOD":
                        continue
                    
                    q, no, timePart, scan, az, el, gain, level, freq, snr, lost, BER, mod = parts
                    
                    no = int(no)
                    
                    scan = int(scan)
                    
                    az = int(az)
                    
                    el = int(el)
                    
                    gain = int(gain)
                    
                    level = int(level)
                    
                    freq = float(freq)
                    
                    snr = float(snr)
                    
                    lost = int(lost)
                    
                    BER = 1 if BER == "N/D" else float(BER)
                    
                    startTimeMoment = datetime.strptime(startDatePart + " " + startTimePart, self.config.timeFormat)
                    
                    timeMoment = datetime.strptime(startDatePart + " " + timePart, self.config.timeFormat)
                    
                    if startTimeMoment.time() > time(hour=23, minute=0, second=0, microsecond=0) and \
                        timeMoment.time() < time(hour=1, minute=0, second=0, microsecond=0):
                            timeMoment += timedelta(days=1)
                    
                    logLines.append(ScanexLogLineModel(
                        timeMoment=timeMoment,
                        azimuth=az,
                        elevation=el,
                        level=level,
                        snr=snr,
                        BER=BER,
                        q=q,
                        no=no,
                        scan=scan,
                        gain=gain,
                        frequency=freq,
                        lost=lost,
                        mod=mod                        
                    ))
                except Exception as e:
                    raise e
            
            startTime = datetime.strptime(startDatePart + " " + startTimePart, self.config.timeStartFormat)
            
            return ScanexLogModel(
                station=station,
                lat=lat,
                lon=lon,
                TLE1=TLE1,
                TLE2=TLE2,
                satellite=satellite,
                configuration=format,
                startTime=startTime,
                demodulator=demodulator,
                recieverID=recieverID,
                plannerVersion=plannerVersion,
                antennaVersion=antennaVersion,
                recieverVersion=recieverVersion,
                hostName=hostName,
                ip0=ip0,
                ip1=ip1,
                demodIP=demodIP,
                username=username,
                domain=domain,
                SID=SID,
                modulation=modulation,
                clock=clock,
                carrier=carrier,
                frameSize=frameSize,
                BERBase=BERBase,
                FEC=FEC,
                swappedIQ=swappedIQ,
                invertedI=invertedI,
                invertedQ=invertedQ,
                scrambling=scrambling,
                diffCoding=diffCoding,
                frequencyOffset=frequencyOffset,
                frequency=frequency,
                diskFree=diskFree,
                firstBuffSize=firstBuffSize,
                secondBuffSize=secondBuffSize,
                lines=logLines                
            )