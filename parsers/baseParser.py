import abc
from pathlib import Path
from models.baseLogModel import BaseLogModel
from config import LogFormat



class BaseParser(abc.ABC):
    name: str = "base"
    config: LogFormat
    
    def __init__(self, config: LogFormat):
        self.config = config
    
    @abc.abstractmethod
    def parse(self, file: Path) -> BaseLogModel:
        ...