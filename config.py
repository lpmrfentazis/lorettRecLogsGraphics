from pydantic import BaseModel
from annotated_types import Annotated, MinLen, MaxLen, Ge, Le



class LogFormat(BaseModel):
    name: Annotated[str, MinLen(1), MaxLen(50)]
    regex: Annotated[str, MinLen(1)]
    keySeparator: Annotated[str, MinLen(1)] = ": "
    elementSeparator: Annotated[str, MinLen(1)]
    timeStartFormat: str
    timeFormat: str
    lonlatPattern: str
    toZiroWhenLoner: Annotated[float, Ge(0)] = 4
    
class GridConfig(BaseModel):
    stepY: Annotated[float, Ge(0.1)] = 0.5
    minY: Annotated[float, Ge(0)] = 0

class GraphicsConfig(BaseModel):
    maxLinesCount: Annotated[int, Ge(1)] = 5
    grid: GridConfig = GridConfig()
    strokeWidth: Annotated[int, Ge(1)] = 3
    # curved: bool = True

# class FletConfig(BaseModel):
#     port: Annotated[int, Ge(1), Le(65535)] = 61245

class ConfigModel(BaseModel):
    formats: list[LogFormat] = [
        LogFormat(name="lorett",
                     regex=r"^.*__\d{8}_\d{6}_.*$",
                     elementSeparator="\t",
                     timeStartFormat="%Y-%m-%d %H:%M:%S",
                     timeFormat="%Y-%m-%d %H:%M:%S.%f",
                     lonlatPattern=r"(\d+\.\d+) lon (\d+\.\d+) lat"
                     ),
        LogFormat(name="scanex",
                     regex=r'^[A-Za-z0-9 ]{1,50}_[0-9]{5}_[0-9]{12}_[0-9]{1,5}[A-Za-z].*$',
                     elementSeparator=" ",
                     timeStartFormat="%Y-%m-%d %H:%M:%S",
                     timeFormat="%Y-%m-%d %H:%M:%S",
                     lonlatPattern=r"Lat. (\d+\.\d+)  Long. (\d+\.\d+)")
    ]
    # flet: FletConfig = FletConfig()
    graphics: GraphicsConfig = GraphicsConfig()


default = ConfigModel()