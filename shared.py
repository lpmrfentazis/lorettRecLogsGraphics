import flet as ft


pallete = [ft.colors.LIGHT_GREEN_500,
           ft.colors.LIGHT_BLUE_400,
           ft.colors.ORANGE_400,
           ft.colors.RED_500,
           ft.colors.PINK_400,
           ft.colors.YELLOW_400,
           ft.colors.PURPLE_400,
           ft.colors.LIME_400,
           ft.colors.DEEP_PURPLE_400,
           ft.colors.INDIGO_400,
           ft.colors.DEEP_ORANGE_400,
           ft.colors.BLUE_400,
           ft.colors.CYAN_400,
           ft.colors.TEAL_400,
           ft.colors.GREEN_400,
           ft.colors.AMBER_400,
           ]    