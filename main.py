from config import ConfigModel, default
import flet as ft
from loader import LoaderMaster



def main(page: ft.Page, config: ConfigModel) -> None:
    page.window.width = 1200
    page.window.height = 800
    page.window.min_width = 1200
    page.window.min_height = 800
    # page.window.icon = ft.Icon(ft.icons.SSID_CHART, color=ft.colors.LIGHT_BLUE_400, size=30)
    page.title = "RecLogsGraphics"
    page.horizontal_alignment = ft.MainAxisAlignment.CENTER
    
    loader = LoaderMaster(root=page, config=config)
   
        
    page.add(loader)



# build me .\venv\Scripts\pyinstaller.exe .\main.py -n "RecLogGraphics" --noconsole -i .\assets\icon.ico
if __name__ == "__main__":
    config = default
    ft.app(lambda p: main(p, config=config))