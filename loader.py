from pathlib import Path
from models.baseLogModel import BaseLogModel
from parsers import getParser, BaseParser
from config import ConfigModel, LogFormat
from typing import Self

from dataclasses import dataclass

import flet as ft
import re
import shared

import random
from datetime import datetime, timedelta


@dataclass
class LoaderUnit:
    format: LogFormat
    pattern: re.Pattern
    parser: BaseParser




class LoaderMaster(ft.Row):
    __preLoaded: list[Path]
    
    loaded: dict[str, BaseLogModel]
    
    config: ConfigModel
    loaders: list[LoaderUnit]
    
    def __init__(self, root: ft.Page, config: ConfigModel) -> Self:
        super().__init__()
        self.config = config
        self.root = root
        
        self.__preLoaded = []
        self.loaded = {}
        
        self.loaders = [
            LoaderUnit(format=i, pattern=re.compile(i.regex), parser=getParser(i)) for i in self.config.formats
        ]
        
        self.pathPicker = ft.FilePicker(on_result= lambda e: self.__onFilePickerResult(e))
         
        self.root.overlay.append(self.pathPicker)
        self.root.update()       
        
        # Left menu render
        # ===============================================================
        self.expand = True
        
        
        # pathField = ft.TextField(value=r"C:\Users\MrFentazis\Downloads\test\meteo", text_align=ft.TextAlign.LEFT, expand=5)
        self.filesSelectedText = ft.Text(value="Selected 0 files", weight=ft.FontWeight.BOLD, text_align=ft.TextAlign.LEFT, expand=6) #FIXME
        
        pathPickerButton = ft.IconButton(icon=ft.icons.FOLDER_OPEN, 
                                        on_click=lambda _: self.pathPicker.pick_files(allow_multiple=True),
                                        expand=2)
        
        pathRow = ft.Row([pathPickerButton, self.filesSelectedText], 1)
        
        self.selectedFilesListView = ft.ListView(expand=True, spacing=1)
        listViewContainer = ft.Container(alignment=ft.alignment.top_center, 
                                    bgcolor=ft.colors.BLACK26,
                                    padding=ft.padding.symmetric(10, 5),
                                    border_radius=ft.border_radius.all(10),
                                    content=self.selectedFilesListView,
                                    expand=3)
        
        
        self.loadButton = ft.OutlinedButton(text="Load logs",
                                            disabled=True, 
                                            expand=True, on_click=lambda e: self.__onClickLoadLogs(e))
        loadButtonRow = ft.Row([ft.Container(self.loadButton, margin=ft.margin.symmetric(0, 10), expand=True)], alignment=ft.MainAxisAlignment.CENTER)
        
        self.loadedFilesText = ft.Text(value="Loaded 0 files", weight=ft.FontWeight.BOLD, text_align=ft.TextAlign.CENTER, expand=1)
        loadedFilesTextRow = ft.Row([self.loadedFilesText], alignment=ft.MainAxisAlignment.CENTER)
        
        loadElementsColumn = ft.Column([loadButtonRow, loadedFilesTextRow], alignment=ft.MainAxisAlignment.START, expand=1)
        
        
        menuColumn = ft.Column([pathRow, listViewContainer, loadElementsColumn], alignment=ft.MainAxisAlignment.START)
        
        menuContainer = ft.Container(alignment=ft.alignment.top_center, 
                                    bgcolor=ft.colors.BLACK26,
                                    margin=ft.margin.Margin(right=30, left=10, top=20, bottom=10),
                                    # padding=ft.padding.symmetric(20, 10),
                                    border_radius=ft.border_radius.all(10),
                                    content=menuColumn,   
                                    expand=1                              
                                    )
        
        # ===============================================================
        # Graphics view render
        self.lineChart = ft.LineChart(
            data_series=[],
            border=ft.border.all(3, ft.colors.with_opacity(0.2, ft.colors.ON_SURFACE)),
            horizontal_grid_lines=ft.ChartGridLines(
                interval=self.config.graphics.grid.stepY, color=ft.colors.with_opacity(0.2, ft.colors.ON_SURFACE), width=1
            ),
            vertical_grid_lines=ft.ChartGridLines(
                interval=self.config.graphics.grid.stepY, color=ft.colors.with_opacity(0.2, ft.colors.ON_SURFACE), width=1
            ),
            tooltip_bgcolor=ft.colors.with_opacity(0.9, ft.colors.BLUE_GREY_600),
            min_y=self.config.graphics.grid.minY,
            min_x=0,
            on_chart_event=self.__tooltip,
            expand=True,
        )
        
        self.chartLegend = ft.Column(height=20)
        # chartTooltip = ft.Column(controls=[ft.Text("sas", size=12)], height=30)
        
        self.chartStack = ft.Stack([self.lineChart,
                                ft.Row([ft.Container(self.chartLegend, margin=ft.margin.Margin(top=10, right=10, left=0, bottom=0))], alignment=ft.MainAxisAlignment.END),   
                                # ft.Row([ 
                                #         ft.Container(expand=1),
                                #         ft.Container(chartTooltip, 
                                #                 margin=ft.margin.symmetric(10, 10), 
                                #                 bgcolor=ft.colors.BLUE_GREY_500,
                                #                 expand=1),
                                #         ft.Container(expand=1)
                                # ], alignment=ft.alignment.bottom_center)
                                ], 
                                visible=False,
                                expand=True)
        
        chartRow = ft.Row([self.chartStack], expand=16)
        
        graphicsColumn = ft.Column([ft.Text(value="Graphics view", weight=ft.FontWeight.BOLD, size=24, expand=1), chartRow], spacing=1, expand=True)
        graphicsContainer = ft.Container(alignment=ft.alignment.top_center, 
                                                    expand=4,
                                                    padding=ft.padding.all(20),
                                                    content=graphicsColumn)
        
        
        
        # ===============================================================
        self.controls = [menuContainer, graphicsContainer]
    
    def __tooltip(self, e: ft.LineChartEvent) -> None:
        ...
        # if e.type == "PointerHoverEvent":
        #     control: ft.LineChart = e.control
        #     line: ft.LineChartData = control.data_series[0]
        #     spots = e.spots
            
        #     point: ft.LineChartDataPoint = line.data_points[spots[0]['spot_index']]
        #     # print(f"hover {control.}")
        #     print(f"hover {point} {spots}")
        # elif e.type == "PointerExitEvent":
        #     print(f"exit")
    
    def __onFilePickerResult(self, e: ft.FilePickerResultEvent) -> None:
        self.__preLoaded = [Path(i.path) for i in e.files]
        
        self.selectedFilesListView.controls = [ft.Text(value=i.name, size=13, text_align=ft.TextAlign.LEFT) for i in self.__preLoaded]
        
        count = len(self.selectedFilesListView.controls)
                    
        self.filesSelectedText.value = f"Selected {count} files"
        
        if count > 0:
            self.loadButton.disabled = False
        else:
            self.loadButton.disabled = True
        
        self.root.update()
        # result = self.loadLogsFromFiles(files=files)
        
    def __onClickLoadLogs(self, e) -> None:
        count = self.loadLogsFromFiles()
        
        if count == 0:
            ...
            return
            
        self.loadedFilesText.value = f"Loaded {count} files"
        
        yMax = 6
        xMax = 0
        
        data = []
        legend = []
        
        # X axis
        start = list(self.loaded.values())[0].startTime
        end = start
        
        for i in self.loaded.values():
            if i.startTime < start:
                start = i.startTime
            
            if len(i.lines) and i.lines[-1].timeMoment > end:
                end = i.lines[-1].timeMoment

        
        for i, name in enumerate(self.loaded.keys()):
            log = self.loaded[name]
            points = [ ft.LineChartDataPoint(x=(line.timeMoment - start).total_seconds(), 
                                             y=line.snr) for line in log.lines if line.snr >= self.config.graphics.grid.minY]
            yMax = max(yMax, max([el.snr for el in log.lines]))
            xMax = max(xMax, len(log.lines))
            
            
            color = shared.pallete[i] if i < len(shared.pallete) else random.choice(shared.pallete)
            
            legend.append(ft.Row([ft.Text(value=name, expand=4), 
                                  ft.Icon(name=ft.icons.SQUARE_ROUNDED, color=color, size=20, expand=1)],
                                 
                                 ))
            
            data.append(ft.LineChartData(data_points=points,
                                         stroke_width=self.config.graphics.strokeWidth,
                                         curved=False,
                                         selected_below_line=True,
                                         color=color))
        
        self.lineChart.data_series = data
        
        self.chartStack.visible = True
        
        self.lineChart.max_y = round(yMax + 3, 0)
        self.lineChart.max_x = round(xMax + 1, -1)
        
        self.chartLegend.controls = legend
        # self.lineChart.
        self.lineChart.left_axis = ft.ChartAxis(show_labels=True, 
                                                labels_interval=self.config.graphics.grid.stepY,
                                                title_size=20,
                                                title=ft.Text(value="SNR", 
                                                              weight=ft.FontWeight.BOLD,
                                                              size=18), 
                                                labels_size=50)
        
        labels = []
        labelsStart = start + timedelta(seconds=5 - (start.second % 5))
        for i in range(5, int((end-labelsStart).total_seconds()), 5):
            labels.append(ft.ChartAxisLabel(value=i, 
                                            label=ft.Text(value=(labelsStart + timedelta(seconds=i)).strftime("%H:%M:%S"), 
                                                          size=11,
                                                          weight=ft.FontWeight.BOLD,
                                                          rotate=30)))
        
        self.lineChart.bottom_axis = ft.ChartAxis(show_labels=True, 
                                                  title_size=20,
                                                  title=ft.Text(value="Time", 
                                                                weight=ft.FontWeight.BOLD,
                                                                size=18), 
                                                  labels=labels,
                                                  labels_size=50)
        
        
        self.root.update()
    
    def loadLogsFromFiles(self) -> int:
        """
            return count of loaded logs
            If no logs could be read (returned 0) self.loaded will not be modified
        """
        loaded = {}
        count = 0
        
        for file in self.__preLoaded:
            for loader in self.loaders:
                if loader.pattern.match(file.name):
                    if not file.exists():
                        continue
                    
                    res = loader.parser.parse(file)
                    
                    if res:
                        loaded[file.name] = res
                        count += 1
        
        if count:
            self.loaded = loaded
            
        return count
        
    def clearLoaded(self) -> None:
        self.loaded = {}
    